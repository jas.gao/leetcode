﻿using System;
using System.Linq;

namespace _14_LongestCommonPrefix
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
			var input = new string[] {"aa", "a"}; 
			var answer = new Solution().LongestCommonPrefix(input);
			Console.Write(answer);
		}
    }


    /**Write a function to find the longest common prefix string amongst an array of strings.

		If there is no common prefix, return an empty string "".

		Example 1:

		Input: ["flower","flow","flight"]
		Output: "fl"
		Example 2:

		Input: ["dog","racecar","car"]
		Output: ""
		Explanation: There is no common prefix among the input strings.
		Note:

		All given inputs are in lowercase letters a-z.
	*/
	public class Solution
	{
		public string LongestCommonPrefix(string[] strs)
		{
			if (!strs.Any()) return "";
			string ans = "";

			for (var i = 0; i < strs[0].Length; i++)
			{
				var c = strs[0][i];
				var found = false;

				foreach (var t in strs)
				{
					if (t.Length > i && t[i] == c)
					{
						found = true;
					}
					else
					{
						found = false;
						break;
					}
				}
				if (found)
				{
					ans += c;
				}
				else
				{
					break;
				}

			}
			return ans;
		}

		public string BetterLongestCommonPrefix(string[] strs)
		{
			int min = 0;
			while (strs.Length > 0)
			{
				foreach (string s in strs)
				{
					if (s.Length == min || s[min] != strs[0][min]) return strs[0].Substring(0, min);
				}
				min++;
			}
			return "";
		}
    }
}
