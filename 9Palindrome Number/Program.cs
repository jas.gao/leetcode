﻿using System;

namespace _9Palindrome_Number
{
    class Program
    {
        static void Main(string[] args)
        {
          var s= new Solution();
		 var answer = s.IsPalindrome(121);
		}
		public class Solution
		{
			public bool IsPalindrome(int x)
			{
				var original = x; 
				if (x < 0)
				{
					return false;
				}

				int revers = 0;

				while (x != 0)
				{
					var pop = x % 10;
					x = x / 10;
					revers = revers * 10 + pop;
				}

				return revers == original;
			}
		}
    }
}
