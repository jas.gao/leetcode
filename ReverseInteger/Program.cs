﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace ReverseInteger
{/***
	Given a 32-bit signed integer, reverse digits of an integer.

	Example 1:

	Input: 123
	Output: 321
	Example 2:

	Input: -123
	Output: -321
	Example 3:

	Input: 120
	Output: 21
	*///
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }

	public class Solution
	{
		public int Reverse(int x)
		{
			try
			{
                bool isMinus = x < 0;
			if (isMinus)
			{
				x = Math.Abs(x);
			}
			string s = x.ToString(); 

			Stack<char> stack = new Stack<char>();

			foreach (var t in s)
			{
				stack.Push(t);
			}

			string reversed = string.Join("", stack);
			
				return isMinus ? -int.Parse(reversed) : int.Parse(reversed);
            }
			catch (OverflowException e)
			{
				return 0;
			}
			


		}

		public int OfficialSolution(int x)
		{
			int rev = 0;
			while (x!=0)
			{
				int pop = x % 10;
				x = x / 10;
				if (rev > int.MaxValue / 10 || (rev == int.MaxValue / 10 && pop > 7)) return 0;
				if (rev < int.MinValue / 10 || (rev == int.MinValue / 10 && pop < -8)) return 0;

				rev = rev * 10 + pop;
			}

			return rev;

		}
	}
}
