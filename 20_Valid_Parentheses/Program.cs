﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _20_Valid_Parentheses
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
			var valid = UsingStack("{}");
			Console.Write(valid);
		}
		/***
		 *	Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

			An input string is valid if:

			Open brackets must be closed by the same type of brackets.
			Open brackets must be closed in the correct order.
			Note that an empty string is also considered valid.

			Example 1:

			Input: "()"
			Output: true
			Example 2:

			Input: "()[]{}"
			Output: true
			Example 3:

			Input: "(]"
			Output: false
			Example 4:

			Input: "([)]"
			Output: false
			Example 5:

			Input: "{[]}"
			Output: true
			*/

		public static bool UsingStack(string s)
		{
			Dictionary<char, char> dic = new Dictionary<char, char> {{'(', ')'}, {'[', ']'}, {'{', '}'}};

			var stack = new Stack<char>();
			foreach (var c in s)
			{
				if (dic.ContainsKey(c))
				{
					stack.Push(c);
				}
				else
				{
					if (stack.Count == 0)
					{
						return false;
					}
					else
					{
						var key = dic.First(p => p.Value == c).Key;

						if (stack.Peek() == key)
						{
							stack.Pop();
						}
						else return false;
					}

					
				}
			}

			return !stack.Any();
		}
    }
}
