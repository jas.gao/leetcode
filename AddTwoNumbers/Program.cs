﻿using System;
using System.Collections.Generic;

namespace AddTwoNumbers
{
    //*
    /*
	 * You are given two non-empty linked lists representing two non-negative integers.
     * The digits are stored in reverse order and each of their nodes contain a single digit.
     * Add the two numbers and return it as a linked list.
     * You may assume the two numbers do not contain any leading zero, except the number 0 itself.
     *
     * Example:
     * Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
     * Output: 7 -> 0 -> 8
     * Explanation: 342 + 465 = 807.
	 */
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");


			Solution solution = new Solution();
			var v1   =  new ListNode(2);
			var v1_1 = new ListNode(4);
			var v1_2 = new ListNode(3);

			v1.next = v1_1;
			v1_1.next = v1_2;

			var v2   = new ListNode(5);
			var v2_1 = new ListNode(6);
			var v2_2 = new ListNode(4);

			v2.next = v2_1;
			v2_1.next = v2_2;
			
			solution.MyAddTwoNumbers(v1, v2);
		}
    }

    
	 //Definition for singly-linked list.
	 public class ListNode
	   {
	     public int val;
	     public ListNode next;
	     public ListNode(int x) { val = x; }
	 }

    public class Solution
	{
		public ListNode AddTwoNumbers(ListNode l1, ListNode l2)
		{
			ListNode dummy = new ListNode(0);
			ListNode curr = dummy;
			int carry = 0;

			while (l1 != null || l2 != null || carry > 0)
			{
				int s = carry;

				if (l1 != null)
				{
					s += l1.val;
					l1 = l1.next;
				}

				if (l2 != null)
				{
					s += l2.val;
					l2 = l2.next;
				}

				if (s > 9)
				{
					carry = 1;
					s = s % 10;
				}
				else
				{
					carry = 0;
				}

				curr.next = new ListNode(s);
				curr = curr.next;
			}

			return dummy.next;



        }

		public ListNode MyAddTwoNumbers(ListNode l1, ListNode l2)
		{
			var dummy = new ListNode(0);
			var current = dummy;
			int sum = 0, carry = 0;
			while (l1!=null && l2!=null)
			{
				sum += carry;
				if (l1 != null)
				{
					sum += l1.val;
					l1 = l1.next;
					
				}

				if (l2 != null)
				{
					sum += l2.val;
					l2 = l2.next;
				}

				if (sum > 9)
				{
					carry = 1;
					sum = sum % carry;
				}
				current.next = new ListNode(sum+carry);
				current = current.next;
			}

			return dummy.next;
		}


        private IEnumerable<ListNode> GetNext(ListNode listNode)
		{
			if (listNode.next != null)
			{
				GetNext(listNode.next);
			}

			yield return listNode;
		}
	}

}
