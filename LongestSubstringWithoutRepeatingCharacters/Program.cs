﻿using System;
using System.Collections.Generic;
using System.Dynamic;

namespace LongestSubstringWithoutRepeatingCharacters
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

		
    }

	public class Solution
	{
		public int LengthOfLongestSubstring(string s)
		{
			int n = s.Length; 
			HashSet<char> set = new HashSet<char>();
			int ans = 0, i = 0, j = 0;
			while (i<n && j<n) 
			{
				if (!set.Contains(s[j]))
				{
					set.Add(s[j++]);
					ans = Math.Max(ans, j - i); 

				}
				else
				{
					set.Remove(s[i++]);
				}
			}

			return ans;


		}

		public int MyVersionLengthOfLongestSubstring(string s)
		{
			var length = s.Length;
			int i = 0, ans = 0, j = 0;
			HashSet<char> set = new HashSet<char>();
			while (i < length && j<length)  
			{
				if (!s.Contains(s[j]))
				{
					set.Add(s[j++]);
					ans = Math.Max(ans, j - i);
				}
				else
				{
					set.Remove(s[i++]);
				}
			}

			return ans;

		}

    }


}
