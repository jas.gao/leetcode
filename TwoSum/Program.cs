﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace TwoSum
{
    /**
	 *Given an array of integers, return indices of the two numbers such that they add up to a specific target.
     * You may assume that each input would have exactly one solution, and you may not use the same element twice.
     * Example:
     * Given nums = [2, 7, 11, 15], target = 9,
     * Because nums[0] + nums[1] = 2 + 7 = 9,
     * return [0, 1].
	 *
	 */
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

			var input = new int[] {1, 2, 4, 5, 6};
			var t = TwoSum(input, 8);
		}

		public static int[] TwoSum(int[] input, int target)
		{

			for (var i = 0; i < input.Length; i++)
			{

				for (var j = i + 1; j < input.Length; j++)
				{
					if (input[i] + input[j] == target)
					{
						return new[] {i,j};
					}
				}
			}

			return null;

		}

		public static int[] TwoSumWithHash(int[] nums, int target)
		{
			var seen = new Dictionary<int, int>();

			for (int i = 0; i < nums.Length; i++)
			{
				int goal = target - nums[i];                       

				if (seen.ContainsKey(goal))
					return new int[] { seen[goal], i };

				if (!seen.ContainsKey(nums[i]))
					seen.Add(nums[i], i);
			}

			return new int[] { };
        }

		/**
		*Given an array of integers, return indices of the two numbers such that they add up to a specific target.
		* You may assume that each input would have exactly one solution, and you may not use the same element twice.
		* Example:
		* Given nums = [2, 7, 11, 15], target = 9,
		* Because nums[0] + nums[1] = 2 + 7 = 9,
		* return [0, 1].
		*
		*/

        public int[] MySolution(int[] input, int target)
		{
			var dictionary = new Dictionary<int, int>();

			for (int i = 0; i < input.Length; i++)
			{
				int goal = target - input[i];
				if (dictionary.ContainsKey(goal))
				{
					return new int[] {dictionary[goal], i};
				}
				else
				{
					dictionary.Add(input[i],i );
				}
			}

			return new int[] { };
		}

    }


}
