﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LeetCodePractices
{
    class Program
    {
        static void Main(string[] args)
        {
           var solution = new Solution();
			var array = new int[3] {1, 1, 2};

			var output = solution.RemoveDuplicates(array);
		}
    }

	public class Solution
	{
		public int RemoveDuplicates(int[] nums)
		{
			var t = new List<int>();
			var temp = nums[0];
			t.Add(temp);
			for (int i = 1; i <= nums.Count() - 1; i++)
			{
				if (nums[i] != temp)
				{
					temp = nums[i];
					t.Add(temp);
				}
				
			};
			nums = t.ToArray();
			return nums.Length;
		}
	}
}
